package com.gamepricemonitor.grabber;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrabberApplication implements CommandLineRunner {

  private static final Logger LOGGER = LoggerFactory.getLogger(GrabberApplication.class);

  public static void main(String[] args) {
    SpringApplication.run(GrabberApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
  }
}
